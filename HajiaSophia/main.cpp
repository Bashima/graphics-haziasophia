//
//  main.cpp
//  HajiaSophia
//
//  Created by Bashima Islam on 11/8/15.
//  Copyright © 2015 Bashima Islam. All rights reserved.
//


#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<GLUT/glut.h>
#include<OpenGL/OpenGL.h>

#define ASH 0.5, .7, 0.9

//make a global variable -- for tracking the anglular position of camera
double cameraAngle;			//in radian
double cameraAngleDelta;

double cameraHeight;
double cameraRadius;

double a[3];
double b[3];
double c[3];

double height, r;
double horseAngle;
double horseSign;
double horseZ;
double PI= 3.1416;

double rectAngle;	//in degree

bool canDrawGrid;

bool cameraView;

void customCube()
{
    glPushMatrix();
    {
        glBegin(GL_QUADS);{
            glVertex3f(13,15,0);
            glVertex3f(13,-15,0);
            glVertex3f(-13,-15,0);
            glVertex3f(-13,15,0);
        }glEnd();
        glBegin(GL_QUADS);{
            glVertex3f(-13,15,0);
            glVertex3f(-13,15,38);
            glVertex3f(-13,-15,38);
            glVertex3f(-13,-15,0);
        }glEnd();
        glBegin(GL_QUADS);{
            glVertex3f(13,15,0);
            glVertex3f(13,15,38);
            glVertex3f(13,-15,38);
            glVertex3f(13,-15,0);
        }glEnd();
        glBegin(GL_QUADS);{
            glVertex3f(-13,15,0);
            glVertex3f(-13,15,38);
            glVertex3f(13,15,38);
            glVertex3f(13,15,0);
        }glEnd();
        glBegin(GL_QUADS);{
            glVertex3f(-13,-15,0);
            glVertex3f(-13,-15,38);
            glVertex3f(13,-15,38);
            glVertex3f(13,-15,0);
        }glEnd();
        glBegin(GL_QUADS);{
            glVertex3f(13,15,38);
            glVertex3f(13,-15,38);
            glVertex3f(-13,-15,38);
            glVertex3f(-13,15,38);
        }glEnd();
    }
    glPopMatrix();
}
void middleSquare()
{
    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        customCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(0, 0, 38);
        glScalef(1, 1, .02);
        glColor3f(.15, .15, .15);
        customCube();
    }
    glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 39);
//        glRotatef(270, 1, 0, 0);
        glColor3f(.46, .46, .46);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
//            glRotatef(0, 0, 0, 0);
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluSphere(quad1, 12.5, 100, 100);
        }glDisable(GL_CLIP_PLANE0);
    }glPopMatrix();

    glPushMatrix(); {
        glTranslatef(0, 0, 39);
        //        glRotatef(270, 1, 0, 0);
        glColor3f(.15, .15, .15);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            //            glRotatef(0, 0, 0, 0);
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            glutWireSphere(12.6, 50, 25);
        }glDisable(GL_CLIP_PLANE0);
    }glPopMatrix();
    glPushMatrix();{
        glScaled(1, 1, .95);
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=3;
        double z = 52;

        x1 = 0,y1=0;
        glColor3f(1.0,1.0,1);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 50);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 3, 3, 2, 10, 10);

    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 52);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 2.5, 2.5, 2.5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=2.5;
        double z = 54.5;

        x1 = 0,y1=0;
        glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 52);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 2.5, 1, 7, 10, 10);

    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 59);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 1, 1, 1.5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=1.5;
        double z = 60.5;

        x1 = 0,y1=0;
        glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 60.5);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 1.5, 1.5, 2.5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=1.5;
        double z = 63;

        x1 = 0,y1=0;
        glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 63);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, .5, .5, 5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=1.5;
        double z = 68;

        x1 = 0,y1=0;
       glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 68);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, 1.5, 1.5, 1.5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=1.5;
        double z = 69.5;

        x1 = 0,y1=0;
        glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, 0, 69.5);
        glColor3f(1, 0.84, 0);
        GLUquadric *quad;
        quad = gluNewQuadric();
        gluCylinder(quad, .5, .5, 5, 10, 10);

    }glPopMatrix();
    glPushMatrix();{
        float x1,y1,x2,y2;
        float angle;
        double radius=.5;
        double z = 74.5;

        x1 = 0,y1=0;
        glColor3f(1, 0.84, 0);
        glRotatef(133, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(x1,y1,z);

        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
        {
            x2 = x1+sin(angle)*radius;
            y2 = y1+cos(angle)*radius;
            glVertex3f(x2,y2,z);
        }glEnd();
    }glPopMatrix();
    glPushMatrix(); {
        glTranslatef(0, -.5, 75.5);
        glRotatef(270, 1, 0, 0);
        glColor3f(1, 0.84, 0);
        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = .25;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            glRotatef(0, 0, 0, 0);
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluCylinder(quad1, 1, 1, 1, 100, 100);
        }glDisable(GL_CLIP_PLANE0);
    }glPopMatrix();
    }glPopMatrix();
}


void drawFilledCircle(GLfloat x, GLfloat y, GLfloat radius,double rt,double rt1,double rt3){
    int i;
    int triangleAmount = 20; //# of triangles used to draw circle

    //GLfloat radius = 0.8f; //radius
    GLfloat twicePi =  PI;
    glPushMatrix();{
    glTranslatef(rt1, rt, rt3+10);
    glRotatef(90, 1, 0, 0);
    glBegin(GL_TRIANGLE_FAN);

    glVertex2f(x, y); // center of circle
    for(i = 0; i <= triangleAmount;i++) {
        glVertex2f(
                   x + (radius * cos(i *  twicePi / triangleAmount)),
                   y + (radius * sin(i * twicePi / triangleAmount))
                   );
    }
    glEnd();
    }glPopMatrix();
}


void makeWindow(double x,double y,double z)
{
    glPushMatrix();
    {
        glScalef(.4, 1, .4);
        glColor3f(0, 0, 0);
        glBegin(GL_QUADS);{

            glVertex3f(x,y,z);

            glVertex3f(x+5,y,z);

            glVertex3f(x+5,y,z+10);

            glVertex3f(x,y,z+10);

        }glEnd();
        double xt=(x+5)/2;
        drawFilledCircle(2.5, 0, 2.5,y,x,z);
        // drawHollowCircle(0, 10, 10);

    }
    glPopMatrix();
}
void makeWindow()
{
    glPushMatrix();
    double i=0;
    while (true) {
        if(i>=2)break;
        glPushMatrix();
        {
            glColor3f(0.001, 0.081, 0.01);
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = 0;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                glTranslatef(-15, i, 0);
                glRotatef(90, 1, 0, 0);
                GLUquadric *quad;
                quad = gluNewQuadric();

                gluDisk (quad, 4, 5, 18, 1);


            }glDisable(GL_CLIP_PLANE0);
        }
        glPopMatrix();
        i+=.1;

    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-10.5, 1.1, -10);
        glScalef(.2, 1, 10);
        glutSolidCube(2);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-19.5, 1.1, -10);
        glScalef(.2, 1, 10);
        glutSolidCube(2);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-15.1, 1.1, -20);
        glScalef(4.9, 1, .2);
        glutSolidCube(2);
    }
    glPopMatrix();

    
    
    
    
}
void makeBakaCube()
{
    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        glBegin(GL_QUADS);{
            glVertex3f(0,0,0);

            glVertex3f(9,0,0);

            glVertex3f(9,0,20);

            glVertex3f(0,0,20);


        }glEnd();


        glBegin(GL_QUADS);{
            glVertex3f(0,10,0);

            glVertex3f(9,10,0);

            glVertex3f(9,10,28);

            glVertex3f(0,10,28);


        }glEnd();



        glBegin(GL_QUADS);{
            glVertex3f(9,0,20);

            glVertex3f(9,0,0);

            glVertex3f(9,10,0);

            glVertex3f(9,10,28);


        }glEnd();

        glBegin(GL_QUADS);{
            glVertex3f(0,0,20);

            glVertex3f(0,0,0);

            glVertex3f(0,10,0);

            glVertex3f(0,10,28);


        }glEnd();

        glColor3f(.16, .16, .16);
        glBegin(GL_QUADS);{
            glVertex3f(0,0,20);

            glVertex3f(9,0,20);

            glVertex3f(9,10,28);

            glVertex3f(0,10,28);
            
            
        }glEnd();


    }
    glPopMatrix();
}


void makeHexa()
{

    glPushMatrix();
    {
        glPushMatrix();
        glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{


            glVertex3f(0,-50,0);

            glVertex3f(15,-50,0);

            glVertex3f(15,-50,30);

            glVertex3f(0,-50,30);


        }glEnd();
//        // makeWindow(0, -50.1, 0);
//        makeWindow(5, -50.1, 15);
//        makeWindow(15, -50.1, 15);
//        makeWindow(25, -50.1, 15);
//
//
//        makeWindow(5, -50.1, 35);
//        makeWindow(15, -50.1, 35);
//        makeWindow(25, -50.1,35);
//
//        makeWindow(5, -50.1, 55);
//        makeWindow(15, -50.1, 55);
//        makeWindow(25, -50.1, 55);

        glPopMatrix();


        glPushMatrix();
         glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(0,-50,0);

            glVertex3f(-10,-35,0);

            glVertex3f(-10,-35,30);

            glVertex3f(0,-50,30);

        }glEnd();
//
//        makeWindow(5, -50.1, 15);
//        makeWindow(15, -50.1, 15);
//        makeWindow(25, -50.1, 15);
        glPopMatrix();




        glPushMatrix();
         glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(15,-50,0);

            glVertex3f(25,-35,0);

            glVertex3f(25,-35,30);

            glVertex3f(15,-50,30);

        }glEnd();
        glPopMatrix();
        glPushMatrix();
        glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(0,-20,0);

            glVertex3f(15,-20,0);

            glVertex3f(15,-20,30);

            glVertex3f(0,-20,30);

        }glEnd();
        glPopMatrix();
        glPushMatrix();
         glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(0,-20,0);

            glVertex3f(-10,-35,0);

            glVertex3f(-10,-35,30);

            glVertex3f(0,-20,30);

        }glEnd();
        glPopMatrix();

        glPushMatrix();
         glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(15,-20,0);

            glVertex3f(25,-35,0);

            glVertex3f(25,-35,30);

            glVertex3f(15,-20,30);

        }glEnd();

//        makeWindow(5, -19.9, 15);
//        makeWindow(15, -19.9, 15);
//        makeWindow(25, -19.9, 15);
//
//
//        makeWindow(5, -19.9, 35);
//        makeWindow(15, -19.9, 35);
//        makeWindow(25, -19.9,35);
//
//        makeWindow(5, -19.9, 55);
//        makeWindow(15, -19.9, 55);
//        makeWindow(25, -19.9, 55);

        glPopMatrix();

        glPushMatrix();
         glColor3f(.46, .46, .46);
        glBegin(GL_QUADS);{

            glVertex3f(0,-50,30);

            glVertex3f(-10,-35,30);

            glVertex3f(25,-35,30);

            glVertex3f(15,-50,30);

        }glEnd();

        glBegin(GL_QUADS);{

            glVertex3f(0,-20,30);

            glVertex3f(-10,-35,30);

            glVertex3f(25,-35,30);

            glVertex3f(15,-20,30);

        }glEnd();
        glPopMatrix();



    }
    glPopMatrix();

}

void makeHalfSphere()
{
    glPushMatrix(); {
        glTranslatef(7, -35, 31);
        //        glRotatef(270, 1, 0, 0);
        glColor3f(.16, .16, .16);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            //            glRotatef(0, 0, 0, 0);
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluSphere(quad1, 12.5, 100, 100);
        }glDisable(GL_CLIP_PLANE0);
    }glPopMatrix();

}


void makeLittleOne()
{
    makeHexa();
    makeHalfSphere();
    
}

void makeMinar()
{
    glPushMatrix(); {

        glTranslatef(0, 0, -60);

        glColor3f(.76, .76, 0.76);

        GLUquadric *quad= gluNewQuadric();

        gluCylinder(quad, 3, 3, 50, 20, 20);


    }glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(0, 0, -10);
        glColor3f(.16, .16, .16);
        glutSolidCone(3,15,20,20);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(0, 0, -57);

        //glTranslatef(0, 0, 39);
        //        glRotatef(270, 1, 0, 0);
        glColor3f(.76, .76, .76);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            //            glRotatef(0, 0, 0, 0);
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            glutSolidCone(5,14,18,20);
        }glDisable(GL_CLIP_PLANE0);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(.76, .76, .76);
        glTranslatef(0, 0, -79);
        glScalef(1, 1, -5);
        glutSolidCube(9);
    }
    glPopMatrix();
    glPopMatrix();
//    glPushMatrix();{
//        float x1,y1,x2,y2;
//        float angle;
//        double radius=5;
//        double z = 100;
//
//        x1 = 0,y1=0;
//        glColor3f(1, 0.84, 0);
//        glRotatef(133, 0, 0, 1);
//        glBegin(GL_TRIANGLE_FAN);
//        glVertex3f(x1,y1,z);
//
//        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
//        {
//            x2 = x1+sin(angle)*radius;
//            y2 = y1+cos(angle)*radius;
//            glVertex3f(x2,y2,z);
//        }glEnd();
//    }glPopMatrix();
//
//    glPushMatrix(); {
//        glTranslatef(0, 0, 100);
//        glColor3f(1, 0.84, 0);
//        GLUquadric *quad;
//        quad = gluNewQuadric();
//        gluCylinder(quad, 1, 15, 15, 10, 10);
//
//    }glPopMatrix();
//    glPushMatrix();{
//        float x1,y1,x2,y2;
//        float angle;
//        double radius=15;
//        double z = 100;
//
//        x1 = 0,y1=0;
//        glColor3f(1, 0.84, 0);
//        glRotatef(133, 0, 0, 1);
//        glBegin(GL_TRIANGLE_FAN);
//        glVertex3f(x1,y1,z);
//
//        for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
//        {
//            x2 = x1+sin(angle)*radius;
//            y2 = y1+cos(angle)*radius;
//            glVertex3f(x2,y2,z);
//        }glEnd();
//    }glPopMatrix();
//

    glPushMatrix();{
        glTranslatef(-42.75, 39, 47);
        glScaled(.3, .3, .3);

        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=3;
            double z = 52;

            x1 = 0,y1=0;
            glColor3f(1.0,1.0,1);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 50);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 3, 3, 2, 10, 10);

        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 52);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 2.5, 2.5, 2.5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=2.5;
            double z = 54.5;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 52);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 2.5, 1, 7, 10, 10);

        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 59);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 1, 1, 1.5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=1.5;
            double z = 60.5;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 60.5);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 1.5, 1.5, 2.5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=1.5;
            double z = 63;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 63);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, .5, .5, 5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=1.5;
            double z = 68;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 68);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, 1.5, 1.5, 1.5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=1.5;
            double z = 69.5;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);

            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, 0, 69.5);
            glColor3f(1, 0.84, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();
            gluCylinder(quad, .5, .5, 5, 10, 10);

        }glPopMatrix();
        glPushMatrix();{
            float x1,y1,x2,y2;
            float angle;
            double radius=.5;
            double z = 74.5;

            x1 = 0,y1=0;
            glColor3f(1, 0.84, 0);
            glRotatef(133, 0, 0, 1);
            glBegin(GL_TRIANGLE_FAN);
            glVertex3f(x1,y1,z);
            
            for(angle=0.0f ; angle<=3.14159*2.2 ; angle+=0.1)
            {
                x2 = x1+sin(angle)*radius;
                y2 = y1+cos(angle)*radius;
                glVertex3f(x2,y2,z);
            }glEnd();
        }glPopMatrix();
        glPushMatrix(); {
            glTranslatef(0, -.5, 75.5);
            glRotatef(270, 1, 0, 0);
            glColor3f(1, 0.84, 0);
            double equ[4];
            equ[0] = 0;
            equ[1] = 1;
            equ[2] = 0;
            equ[3] = .25;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                glRotatef(0, 0, 0, 0);
                GLUquadric *quad1;
                quad1 = gluNewQuadric();
                gluCylinder(quad1, 1, 1, 1, 100, 100);
            }glDisable(GL_CLIP_PLANE0);
        }glPopMatrix();
    }glPopMatrix();


}
void makeDiskStruct()
{

    glPushMatrix();
    {
        glBegin(GL_QUADS);{
            glVertex3f(0,0,0);
            glVertex3f(0,0,25);
            glVertex3f(65,0,25);
            glVertex3f(65,0,0);
        }glEnd();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glBegin(GL_QUADS);{
            glVertex3f(-30,0,0);
            glVertex3f(-30,0,25);
            glVertex3f(-65,0,25);
            glVertex3f(-65,0,0);
        }glEnd();
    }
    glPopMatrix();



    glPushMatrix();
    {
        double equ1[4];
        equ1[0] = 0;
        equ1[1] = 0;
        equ1[2] = -1;
        equ1[3] = 25;
        glClipPlane(GL_CLIP_PLANE1,equ1);
        glEnable(GL_CLIP_PLANE1); {
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = 0;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                glTranslatef(-15, 0, 0);
                glRotatef(90, 1, 0, 0);
                GLUquadric *quad;
                quad = gluNewQuadric();

                gluDisk (quad, 15, 35, 18, 1);


            }glDisable(GL_CLIP_PLANE0);





        }glDisable(GL_CLIP_PLANE1);




    }

    glPopMatrix();









}

void makeShpericRoof()
{
    glPushMatrix();
    glColor3f(.16, 0.16, .16);
    double equ1[4];
    equ1[0] = 0;
    equ1[1] = 0;
    equ1[2] = -1;
    equ1[3] = 8;
    glClipPlane(GL_CLIP_PLANE1,equ1);
    glEnable(GL_CLIP_PLANE1); {
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluSphere(quad1, 15.5, 100, 100);


        }glDisable(GL_CLIP_PLANE0);





    }glDisable(GL_CLIP_PLANE1);
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(0, 0, 7);
        glColor3f(.16, 0.16, 0.16);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluSphere(quad1, 12.5, 100, 100);



        }glDisable(GL_CLIP_PLANE0);


    }
    glPopMatrix();




}
void makeCylinderRoof()
{

    glPushMatrix();
    {
        glRotatef(90, 0, 0, 0);
        glTranslatef(0, 0, -55);

        glColor3f(.48, .48, .48);



        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad= gluNewQuadric();

            gluCylinder(quad, 15, 15, 33, 20, 20);


        }glDisable(GL_CLIP_PLANE0);


    }
    glPopMatrix();



    glPushMatrix();
    {
        glRotatef(90, 0, 1, 0);
        glTranslatef(0, 39, -16);

        glColor3f(.48, .48, .48);


        double equ[4];
        equ[0] = -1;
        equ[1] = 0;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad= gluNewQuadric();

            gluCylinder(quad, 15, 15, 33, 20, 20);


        }glDisable(GL_CLIP_PLANE0);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(0, 37, 12);
        glColor3f(.16, .16, .16);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad1;
            quad1 = gluNewQuadric();
            gluSphere(quad1, 11, 100, 100);



        }glDisable(GL_CLIP_PLANE0);

    }
    glPopMatrix();



    glPushMatrix();
    {
        glTranslatef(0, 22, 0);
        glRotatef(90, 1, 0, 0);
        glColor3f(1, .50, .44);


        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, .1, 15, 100, 1);



        }glDisable(GL_CLIP_PLANE0);


    }
    glPopMatrix();

    glPushMatrix();
    {
        glRotatef(90, 1, 0, 0);
        glTranslatef(-15, -28, -22);
        glScalef(.6, .6, .5);
        glColor3f(1, .50, .44);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,50,0);
            glVertex3f(50,50,0);
            glVertex3f(50,0,0);
        }glEnd();


    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(0, 55, 0);
        glRotatef(90, 1, 0, 0);
        glColor3f(1, .50, .44);



        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, .1, 15, 100, 1);



        }glDisable(GL_CLIP_PLANE0);



    }
    glPopMatrix();


    glPushMatrix();
    {
        glRotatef(90, 1, 0, 0);
        glTranslatef(-15, -28, -55);
        glScalef(.6, .6, .5);
        glColor3f(1, .50, .44);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,50,0);
            glVertex3f(50,50,0);
            glVertex3f(50,0,0);
        }glEnd();


    }



    glPopMatrix();

    glPushMatrix();
    {
        // glRotatef(90, 1, 0, 0);
        glTranslatef(-16, 39, 0);
        glRotatef(90, 0, 1, 0);
        glColor3f(1, .50, .44);



        double equ[4];
        equ[0] = -1;
        equ[1] = 0;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, .1, 15, 100, 1);



        }glDisable(GL_CLIP_PLANE0);



    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(.48, .48, .58);
        glRotatef(90, 0, 1, 0);
        glTranslatef(0, 24, 17);
        glScalef(.6, .6, .5);

        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,50,0);
            glVertex3f(50,50,0);
            glVertex3f(50,0,0);
        }glEnd();


    }



    glPopMatrix();


    glPushMatrix();
    {
        // glRotatef(90, 1, 0, 0);
        glTranslatef(17, 39, 0);
        glRotatef(90, 0, 1, 0);
        glColor3f(.48, .48, .48);



        double equ[4];
        equ[0] = -1;
        equ[1] = 0;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, .1, 15, 100, 1);



        }glDisable(GL_CLIP_PLANE0);



    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glRotatef(90, 0, 1, 0);
        glTranslatef(0, 24, -16);
        glScalef(.6, .6, .5);

        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,50,0);
            glVertex3f(50,50,0);
            glVertex3f(50,0,0);
        }glEnd();


    }



    glPopMatrix();



}
void makeKothinFrontEnd()
{



    glPushMatrix();
    {
        makeShpericRoof();

    }glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(1, 0,-27);
        glColor3f(1, .45, .39);

        GLUquadric *quad= gluNewQuadric();

        gluCylinder(quad, 16, 16, 20, 20, 20);


    }
    glPopMatrix();


    glPushMatrix();
    {
        glTranslatef(1, 0,-15);
        glColor3f(1, .50, .44);

        GLUquadric *quad= gluNewQuadric();

        gluCylinder(quad, 13, 13, 20, 20, 20);


    }
    glPopMatrix();

    glPushMatrix();
    {

        glPushMatrix(); {

            glTranslatef(-15, -5, -26);
            //        glRotatef(270, 1, 0, 0);
            glColor3f(.16, .16, .16);
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = 0;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                //            glRotatef(0, 0, 0, 0);
                GLUquadric *quad1;
                quad1 = gluNewQuadric();
                gluSphere(quad1, 10, 100, 100);
            }glDisable(GL_CLIP_PLANE0);



        }glPopMatrix();

    }glPopMatrix();
    glPushMatrix();
    {

        glPushMatrix(); {

            glTranslatef(15, -5, -26);
            //        glRotatef(270, 1, 0, 0);
            glColor3f(.16, .16, .16);
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = 0;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                //            glRotatef(0, 0, 0, 0);
                GLUquadric *quad1;
                quad1 = gluNewQuadric();
                gluSphere(quad1, 10, 100, 100);
            }glDisable(GL_CLIP_PLANE0);



        }glPopMatrix();

    }glPopMatrix();
    glPushMatrix();
    {
        glColor3f(1, .55, .49);

        glScalef(3.9, 2, 3);
        glTranslatef(-.1, -1, -16);
        glutSolidCube(15);
    }glPopMatrix();
//    glPushMatrix();
//    {
//        glTranslatef(-14.1, -3, -45);
//        glScalef(.7, .7, .7);
//        glRotatef(90, 0, 0, 1);
//        makeCylinderRoof();
//    }
//    glPopMatrix();
//    glPushMatrix();
//    {
//        glTranslatef(67.1, -3, -45);
//        glScalef(.7, .7, .7);
//        glRotatef(90, 0, 0, 1);
//        makeCylinderRoof();
//    }
//    glPopMatrix();




}
void makeSubKothinFront()
{
    //{
    glPushMatrix();
    {
        glTranslatef(.5, .2, 4.6);
        glRotatef(90, 0, 0, -1);
        glColor3f(.16, .16, .16);
        double equ1[4];
        equ1[0] = 1;
        equ1[1] = 0;
        equ1[2] = 0;
        equ1[3] = 5;
        glClipPlane(GL_CLIP_PLANE1,equ1);
        glEnable(GL_CLIP_PLANE1); {
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = 0;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                GLUquadric *quad;
                quad = gluNewQuadric();

                gluSphere(quad, 10, 30, 40) ;
            }glDisable(GL_CLIP_PLANE0);

        }glDisable(GL_CLIP_PLANE1);
    }
    glPopMatrix();




    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        double equ[4];
        equ[0] = 0;
        equ[1] = -1;
        equ[2] = 0;
        equ[3] = 3;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad= gluNewQuadric();

            gluCylinder(quad, 10, 10, 5, 20, 20);
        }glDisable(GL_CLIP_PLANE0);

    }
    glPopMatrix();



    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glScalef(2.5, 1.5, .9);
        glTranslatef(0, 5, 1.6);
        glutSolidCube(8);
    }
    glPopMatrix();


    glPushMatrix();
    {

        glColor3f(1, .50, .44);
        glTranslatef(8, 1.3, 8);
        glRotatef(60, 0, 1, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,9,0);
            glVertex3f(4,9,0);
            glVertex3f(4,0,0);
        }glEnd();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glTranslatef(-10, 1.3, 5);
        glRotatef(60, 0, -1, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,9,0);
            glVertex3f(4,9,0);
            glVertex3f(4,0,0);
        }glEnd();
    }
    glPopMatrix();

    ////////////////////////
    glPushMatrix();
    {
        glColor3f(.16, .16, .16);
        glTranslatef(8, 1.3, 8);
        glRotatef(60, 0, -1, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,9,0);
            glVertex3f(4,9,0);
            glVertex3f(4,0,0);
        }glEnd();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(.16, .16, .16);
        glTranslatef(-10, 1.3, 11.5);
        glRotatef(60, 0, 1, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,9,0);
            glVertex3f(4,9,0);
            glVertex3f(4,0,0);
        }glEnd();
    }
    glPopMatrix();

    /////////////////////////////
    glPushMatrix();
    {
        glRotatef(90, 0, 0, 0);
        glTranslatef(0, 1.4, -13);

        glColor3f(.16, .16, .16);



        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = -10;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad= gluNewQuadric();

            gluCylinder(quad, 14.2, 14.2, 11, 20, 20);


        }glDisable(GL_CLIP_PLANE0);


    }
    glPopMatrix();


    glPushMatrix();
    {
        glColor3f(.16, .16, .16);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = -9;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            glTranslatef(0, 2, 5);
            glRotatef(90, 1, 0, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, 8, 11.5, 18, 1);


        }glDisable(GL_CLIP_PLANE0);



    }
    glPopMatrix();



    /////////////////////
    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        glScalef(1.5, .5, 5);
        glTranslatef(.7, -18, -4);
        glutSolidCube(8);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glScalef(3.5, 1.5, 5);
        glTranslatef(0, 5, -4);
        glutSolidCube(8);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(60, 0, 0,1);
        glColor3f(1, .50, .44);
        glScalef(2, .5, 5);
        glTranslatef(2,-20, -4);
        glutSolidCube(8);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(60, 0, 0,-1);
        glColor3f(1, .50, .44);
        glScalef(2, .5, 5);
        glTranslatef(-1,-20, -4);
        glutSolidCube(8);
    }
    glPopMatrix();


}
void makeSide1()
{
    glPushMatrix();
    {
        glScalef(.6, .6, .6);
        makeKothinFrontEnd();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glScalef(.6, .6, .6);
        glTranslatef(1, -26, -30);
        makeSubKothinFront();

    }
    glPopMatrix();

}

void makeHabijabiSide2()
{
    glPushMatrix();
    {
        glTranslatef(1, 1, -25);
        glPushMatrix();
        {
            glRotatef(90, 0, 0, 0);
            glTranslatef(0, 1.6, 9);

            glColor3f(.46, .46, .46);



            double equ[4];
            equ[0] = 0;
            equ[1] = 1;
            equ[2] = 0;
            equ[3] = -10;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                GLUquadric *quad= gluNewQuadric();

                gluCylinder(quad, 14.2, 14.2, 11, 20, 20);


            }glDisable(GL_CLIP_PLANE0);


        }
        glPopMatrix();
        glPushMatrix();
        {
            glColor3f(.16, .16, .16);
            double equ[4];
            equ[0] = 0;
            equ[1] = 0;
            equ[2] = 1;
            equ[3] = -11;
            glClipPlane(GL_CLIP_PLANE0,equ);
            glEnable(GL_CLIP_PLANE0); {
                glTranslatef(0, -20, 5);
                glRotatef(90, 1, 0, 0);
                GLUquadric *quad;
                quad = gluNewQuadric();

                gluDisk (quad, 10, 11.5, 18, 1);


            }glDisable(GL_CLIP_PLANE0);



        }
        glPopMatrix();

    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        glTranslatef(-10, -13, -15);
        glScalef(2, 3, 1);
        glutSolidCube(4);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        glTranslatef(10, -13, -15);
        glScalef(2, 3, 1);
        glutSolidCube(4);
    }
    glPopMatrix();

    glPushMatrix();
    {
         glColor3f(1, .55, .49);
        glTranslatef(0, -13, -29);
        glScalef(7, 3, 6);
        glutSolidCube(4);
    }
    glPopMatrix();

    glPushMatrix();
    {
         glColor3f(1, .55, .49);
        glTranslatef(-25, -13, -35);
        glScalef(6.3, 3, 6);
        glutSolidCube(4);
    }
    glPopMatrix();

    glPushMatrix();
    {
         glColor3f(1, .55, .49);
        glTranslatef(24, -13, -35);
        glScalef(6.3, 3, 6);
        glutSolidCube(4);
    }
    glPopMatrix();

    glPushMatrix();
    {
         glColor3f(1, .55, .49);
        glTranslatef(0, -21, -31);
        glScalef(18, 1, 6);
        glutSolidCube(4);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(.46, 0.46, 0.46);
        glTranslatef(-7, -23, -19);
        glRotatef(58, 1, 0, 0);
        glScalef(1.8, 2, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,4,0);
            glVertex3f(9,4,0);
            glVertex3f(9,0,0);
        }glEnd();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(.46, 0.46, 0.46);
        glTranslatef(-35, -27, -25);
        glRotatef(58, 1, 0, 0);
        glScalef(7.8, 2, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,4,0);
            glVertex3f(9,4,0);
            glVertex3f(9,0,0);
        }glEnd();
    }
    glPopMatrix();


    glPushMatrix();
    {
        glColor3f(1, .55, .49);
        glTranslatef(-35, -27, -44);
        glRotatef(90, 1, 0, 0);
        glScalef(7.8, 5, 0);
        glBegin(GL_QUADS);{


            glVertex3f(0,0,0);
            glVertex3f(0,4,0);
            glVertex3f(9,4,0);
            glVertex3f(9,0,0);
        }glEnd();
    }
    glPopMatrix();



}
void makeSide2()
{
    glPushMatrix();
    {
        glScalef(.6, .6, .6);
        makeKothinFrontEnd();
    }
    glPopMatrix();
    glPushMatrix();
    {
        makeHabijabiSide2();
    }
    glPopMatrix();


}

void makeEqualSideHabiJabi()
{

    glPushMatrix();
    makeCylinderRoof();
    glPopMatrix();

    glPushMatrix();
    {
        glScalef(2, 4, 12);
        glTranslatef(10.5, 8, 0);
        glutSolidCube(5);

    }glPopMatrix();


    glPushMatrix();
    {
        glTranslatef(80, 1, 1);
        glPushMatrix();
        makeCylinderRoof();
        glPopMatrix();

        glPushMatrix();
        {
            glScalef(2, 4, 12);
            glTranslatef(-10.5, 8, 0);
            glutSolidCube(5);

        }glPopMatrix();

    }glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(33.5, 1, 1);
        glScalef(.5, .8, 1);
        makeCylinderRoof();

    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(45.5, 1, 1);
        glScalef(.5, .8, 1);
        makeCylinderRoof();

    }
    glPopMatrix();


}
void makeWallDecorator()
{
    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glScalef(3, 4, 2);
        glutSolidCube(3);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glTranslatef(0, 0, 6);
        glScalef(2.5, 4, 2);
        glutSolidCube(3);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glTranslatef(0, 0, 12);
        glScalef(2, 4, 2);
        glutSolidCube(3);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(1, .50, .44);
        glTranslatef(0, -3.4, 18);
        glScalef(1.5, 2, 2);
        glutSolidCube(3);
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(0, -2.5, 21);
        glColor3f(.16, 0.16, 0.16);
        glRotatef(90, 1, 0, 0);
        double equ[4];
        equ[0] = 0;
        equ[1] = 1;
        equ[2] = 0;
        equ[3] = 0;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            GLUquadric *quad= gluNewQuadric();

            gluCylinder(quad, 2.1, 2.1, 4, 20, 20);


        }glDisable(GL_CLIP_PLANE0);

    }
    glPopMatrix();
    glPushMatrix();

    {
        glColor3f(.16, 0.16, 0.16);
        double equ[4];
        equ[0] = 0;
        equ[1] = 0;
        equ[2] = 1;
        equ[3] = -20;
        glClipPlane(GL_CLIP_PLANE0,equ);
        glEnable(GL_CLIP_PLANE0); {
            glTranslatef(0, -6, 21);
            glRotatef(90, 1, 0, 0);
            GLUquadric *quad;
            quad = gluNewQuadric();

            gluDisk (quad, .001, 2, 18, 1);

            
        }glDisable(GL_CLIP_PLANE0);
        
    }
    glPopMatrix();
    
    
    
}

void makeUnit()
{
    glPushMatrix();
    {
        glScalef(1, .2, 3);
        glutSolidCube(10);

    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-14, 0, 1);
        glScalef(.6, .2, 3);
        glutSolidCube(10);

    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(15, -1, 1);
        glScalef(.6, .2, 3);
        glutSolidCube(10);

    }
    glPopMatrix();

    glPushMatrix();
    {
        
    }
    glPopMatrix();
}


void makeCrest()
{
    glPushMatrix();
    int r1=7;
    //ArchUnit();
    for (double i=0;i<=360;i+=30)
    {
        glTranslatef(r1*cos(i*3.1416/180),r1*sin(i*3.1416/180),0);
        glPushMatrix();
        glRotatef(i,0,0,1);
        //glScalef(2, 1, 1);
        glPushMatrix();
        glScalef(2, 2, 2);
        glutSolidCube(1.5);

        glPopMatrix();
        glPopMatrix();
    }
    glPopMatrix();

}

void makeCrest2()
{
    glPushMatrix();
    int r1=8;
    //ArchUnit();
    for (double i=0;i<=360;i+=30)
    {
        glTranslatef(r1*cos(i*3.1416/180),r1*sin(i*3.1416/180),0);
        glPushMatrix();{
        glRotatef(i,0,0,1);
        //glScalef(2, 1, 1);

        glTranslatef(0, 0, -2);
        glScalef(.1, .1, .1);
        makeUnit();
        }
        glPopMatrix();
    }
    glPopMatrix();

}

void makeWall2()
{
    glPushMatrix();


    double ind=0;
    // for (int i=0; i<=5; i+=1) {
    while (true) {
        ind+=.1;
        if(ind>=5)break;
        glPushMatrix();
        {

            glColor3f(.16,.16,.16);
            glTranslatef(0, ind, 0);
            makeDiskStruct();


        }
        glPopMatrix();
    }

    //  }



    glPushMatrix();
    {

        glTranslatef(-15, 2.8, 1);
        glScalef(10, 2, .5);
        glutSolidCube(3);
    }
    glPopMatrix();
    glPopMatrix();


}
void clippedWall()
{
    glPushMatrix();
    ;
    glColor3f(.16, .16, .16);
    double equ1[4];
    equ1[0] = 1;
    equ1[1] = 0;
    equ1[2] = 0;
    equ1[3] = 17;
    glClipPlane(GL_CLIP_PLANE3,equ1);
    glEnable(GL_CLIP_PLANE3); {
        double equ[4];
        equ[0] = -1;
        equ[1] = 0;
        equ[2] = 0;
        equ[3] = 2;
        glClipPlane(GL_CLIP_PLANE2,equ);
        glEnable(GL_CLIP_PLANE2); {
            glColor3f(.16, .16, .16);
            makeWall2();


        }glDisable(GL_CLIP_PLANE2);


    }glDisable(GL_CLIP_PLANE3);
    glPopMatrix();


}





void makeEqualSide()
{
    glPushMatrix();
    {
        glScalef(.5, .5, .5);
        makeEqualSideHabiJabi();


    }
    glPopMatrix();
    glPushMatrix();
    {
        glScalef(.5, .8, .8);
        glTranslatef(21.5, 19, 17);
        makeWallDecorator();

    }glPopMatrix();

    glPushMatrix();
    {
        glScalef(.5, .8, .8);
        glTranslatef(59, 19, 17);
        makeWallDecorator();

    }glPopMatrix();

}

void makeWall1()
{
    glPushMatrix();
    {
        glColor3f(.46  , .46, .46);
        makeDiskStruct();

    }
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(.46  , .46, .46);
        glTranslatef(0, 5, 0);
        makeDiskStruct();
    }
    //  for(int i=0;i>=-5;i-=1)
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(.46  , .46, .46);
        glTranslatef(0, 1, 0);
        makeDiskStruct();

    }
    glPopMatrix();

    double ind=0;
    // for (int i=0; i<=5; i+=1) {
    while (true) {
        ind+=.1;
        if(ind>=5)break;
        glPushMatrix();
        {

            glColor3f(.16  , .16, .16);
            glTranslatef(0, ind, 0);
            makeDiskStruct();


        }
        glPopMatrix();
    }

    //  }



    glPushMatrix();
    glColor3f(.46, 0.46, 0.46);
    glBegin(GL_QUADS);{
        glVertex3f(-65,0,25.1);
        glVertex3f(-65,5,25.1);
        glVertex3f(65,5,25.1);
        glVertex3f(65,0,25.1);
    }glEnd();
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(65, 2.6, 12);
        glScalef(1, 2.5, 13);
        glutSolidCube(2);
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-65, 2.6, 12);
        glScalef(1, 2.5, 13);
        glutSolidCube(2);
    }
    glPopMatrix();
    
    
}

void makeWall3()
{
    glPushMatrix();
    {
        glColor3f(0, 1, 0);
        makeDiskStruct();

    }
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(1, 1, 0);
        glTranslatef(0, 5, 0);
        makeDiskStruct();
    }
    //  for(int i=0;i>=-5;i-=1)
    glPopMatrix();
    glPushMatrix();
    {
        glColor3f(1, 1, 0);
        glTranslatef(0, 1, 0);
        makeDiskStruct();

    }
    glPopMatrix();

    double ind=0;
    // for (int i=0; i<=5; i+=1) {
    while (true) {
        ind+=.1;
        if(ind>=5)break;
        glPushMatrix();
        {

            glColor3f(1, 1, 0);
            glTranslatef(0, ind, 0);
            makeDiskStruct();


        }
        glPopMatrix();
    }

    //  }



    glPushMatrix();
    {
        glTranslatef(-15, 2.8, 1);
        glScalef(10, 2, .5);
        glutSolidCube(3);
    }
    glPopMatrix();


}


void display(){

    //clear the display
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(ASH, 0);	//color ash
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /********************
     / set-up camera here
     ********************/
    //load the correct matrix -- MODEL-VIEW matrix
    glMatrixMode(GL_MODELVIEW);

    //initialize the matrix
    glLoadIdentity();

    //now give three info
    //1. where is the camera (viewer)?
    //2. where is the camera is looking?
    //3. Which direction is the camera's UP direction?

    //instead of CONSTANT information, we will define a circular path.
    	gluLookAt(a[0],a[1],a[2],	b[0],b[1],b[2],	c[0],c[1],c[2]);

//    if (cameraView) {
//        gluLookAt(cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
//    }
//    else{
//        gluLookAt(		0,0,50,	cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraHeight,	0,0,1);
//    }
    //NOTE: the camera still CONSTANTLY looks at the center
    // cameraAngle is in RADIAN, since you are using inside COS and SIN


    //again select MODEL-VIEW
    glMatrixMode(GL_MODELVIEW);


    /****************************
     / Add your objects from here
     ****************************/
    //add objects
    //rotate this rectangle around the Z axis


    //some gridlines along the field
    int i;

    //WILL draw grid IF the "canDrawGrid" is true:

    if(canDrawGrid == true){
        glColor3f(.5, 0.5, 0.5);	//grey
        glBegin(GL_LINES);{
            for(i=-10;i<=10;i++){

                if(i==0)
                    continue;	//SKIP the MAIN axes

                //lines parallel to Y-axis
                glVertex3f(i*10, -100, 0);
                glVertex3f(i*10,  100, 0);

                //lines parallel to X-axis
                glVertex3f(-100, i*10, 0);
                glVertex3f( 100, i*10, 0);
            }
        }glEnd();
        glColor3f(1, 1, 1);	//100% white
        glBegin(GL_LINES);{
            //Y axis
            glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
            glVertex3f(0,  150, 0);

            //X axis
            glVertex3f(-150, 0, 0);
            glVertex3f( 150, 0, 0);
        }glEnd();
    }
    glPushMatrix();
    {
        glScalef(1.2, 1.2, 1.2);
        middleSquare();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(0,-25,0);
        glScalef(1.5, .5, .5);
        glColor3f(1, .50, .44);
        customCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(-20, -45, 14.5);
        makeEqualSide();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(180, 0, 0, 1);
        glPushMatrix();
        {
            glTranslatef(0,-25,0);
            glScalef(1.5, .5, .5);
            customCube();
        }
        glPopMatrix();
        glPushMatrix();
        {
            glTranslatef(-20, -45, 14.5);
            makeEqualSide();
        }
        glPopMatrix();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(90, 0, 0, 1);
        glTranslatef(0, -20, 35);
        glScalef(1, 1, .83);
        makeSide1();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(270, 0, 0, 1);
        glTranslatef(0, -20, 37);
        glScalef(1, 1, .83);
        makeSide2();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(-43, 39, 61);
        glScalef(1, 1, .6);
        makeMinar();
    }
    glPushMatrix();
    {
        glTranslatef(-43, -39, 61);
        glScalef(1, 1, .6);
        makeMinar();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(32, 35, 61);
        glScalef(1, 1, .6);
        makeMinar();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(32, -35, 61);
        glScalef(1, 1, .6);
        makeMinar();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(30, 70, 0);
        glScalef(.5, .5, .7);
        makeLittleOne();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(10, 70, 0);
        glScalef(.5, .5, .7);
        makeLittleOne();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(40, 100, 0);
        glScalef(.5, .5, .7);
        makeLittleOne();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(20, -40, 0);
        glScalef(.3, .3, .5);
        makeLittleOne();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(60, -20, 0);
        glScalef(.3, .3, .5);
        makeLittleOne();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(7, -40, 0);
        glScalef(.5, .7, .7);
        makeBakaCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(-1, -40, 0);
        glScalef(.3, .7, .7);
        makeBakaCube();
    }glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(-12, -40, 0);
        glScalef(.5, .7, .7);
        makeBakaCube();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glRotatef(180, 0, 0, 1);
        glTranslatef(7, -40, 0);
        glScalef(.5, .7, .7);
        makeBakaCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(180, 0, 0, 1);
        glTranslatef(-1, -40, 0);
        glScalef(.3, .7, .7);
        makeBakaCube();
    }glPopMatrix();
    glPushMatrix();
    {
        glRotatef(180, 0, 0, 1);
        glTranslatef(-12, -40, 0);
        glScalef(.5, .7, .7);
        makeBakaCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glRotatef(90, 0, 0, 1);
        glTranslatef(0, -50, 0);
        glScalef(.5, 1, .4);
        makeWall1();
    }
    glPopMatrix();
    glPushMatrix();
    {
//        glRotatef(90, 0, 0, 1);
        glColor3f(.46, .46, .46);
        glTranslatef(42, 35, 0);
        glScalef(.65, .27, .26);
        customCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        //        glRotatef(90, 0, 0, 1);
        glColor3f(.46, .46, .46);
        glTranslatef(42, -35, 0);
        glScalef(.65, .27, .26);
        customCube();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(42.5, 7, 10);
        glRotatef(90, 0, 0, 1);
        glScalef(.4, .4, .3);
//        glColor3f(.46, .46, .46);

//        glScalef(.65, .27, .26);
//        makeWall2();
        makeWindow();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(42.5, 7, 18);
        glRotatef(90, 0, 0, 1);
        glScalef(.4, .4, .3);
        makeWindow();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(30.5, 20, 18);
        glRotatef(90, 0, 0, 1);
        glScalef(.4, .4, .3);
        makeWindow();
    }
    glPopMatrix();

    glPushMatrix();
    {
        glTranslatef(-3, -12.5, 55);
//        glRotatef(90, 0, 0, 1);
//        glScalef(.12, .12, .12);
        makeCrest();
    }
    
    glPopMatrix();

//    glPushMatrix();
//    {
//        glTranslatef(-3, -12.5, 55);
//        //        glRotatef(90, 0, 0, 1);
//        //        glScalef(.12, .12, .12);
//        makeCrest2();
//    }glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(45, 10, 0);
        //        glRotatef(90, 0, 0, 1);
        glScalef(1,.4, .4);
        clippedWall();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(43, 10, 10);
        //        glRotatef(90, 0, 0, 1);
        glScalef(1,.4, .4);
        clippedWall();
    }
    glPopMatrix();


    glPushMatrix();
    {
        glTranslatef(45, -10, 0);
        //        glRotatef(90, 0, 0, 1);
        glScalef(1,.4, .4);
        clippedWall();
    }
    glPopMatrix();
    glPushMatrix();
    {
        glTranslatef(43, -10, 10);
        //        glRotatef(90, 0, 0, 1);
        glScalef(1,.4, .4);
        clippedWall();
    }
    glPopMatrix();

    //ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
    glutSwapBuffers();
}

double sqr(double a)
{
    return a*a;
}
double getLength(double x[3])
{
    return sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
}
double getLength(double x, double y, double z)
{
    return sqrt(sqr(x)+sqr(y)+sqr(z));
}

void walk_fly(int d, bool f)
{
    double da[3];
    // find vector
    if (f) //fly
    {
        for (int i=0; i<3; i++) {
            da[i]=c[i];
        }
    }
    else //walk
    {
        for (int i=0; i<3; i++) {
            da[i]=b[i]-a[i];
        }
    }
    double len= getLength(da);
    //normalize
    for (int i=0; i<3; i++) {
        da[i]=da[i]/len;
    }
    for (int i=0; i<3; i++) {
        da[i] = da[i]*d;
        a[i]=a[i]+da[i];
        b[i]=b[i]+da[i];
    }
}

void starf(int d)
{
    double da[3],dap[3];
    for (int i=0; i<3; i++) {
        dap[i]=b[i]-a[i];
    }
    da[0]=dap[1]*c[2]-dap[2]*c[1];
    da[1]=dap[2]*c[0]-dap[0]*c[2];
    da[2]=dap[0]*c[1]-dap[1]*c[0];
    double len=getLength(da);
    //normalize
    for (int i=0; i<3; i++) {
        da[i]=da[i]/len;
    }
    for (int i=0; i<3; i++) {
        da[i] = da[i]*d;
        a[i]=a[i]+da[i];
        b[i]=b[i]+da[i];
    }
}

void yaw(double d)
{
    double da[3],dd[3];
    for (int i=0; i<3; i++) {
        dd[i]=b[i]-a[i];
    }
    double len= getLength(c);
    for (int i=0; i<3; i++) {
        da[i]=c[i]/len;
    }
    double t=dd[0]*da[0]+dd[1]*da[1]+dd[2]*da[2];
    b[0]=a[0]+cos((double)d)*dd[0]+(1-cos((double)d))*t*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    b[1]=a[1]+cos((double)d)*dd[1]+(1-cos((double)d))*t*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    b[2]=a[2]+cos((double)d)*dd[2]+(1-cos((double)d))*t*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);
    
}
void roll(double d)
{
    double da[3];
    for (int i=0; i<3; i++) {
        da[i]=b[i]-a[i];
    }
    double len=getLength(da);
    //normalize
    for (int i=0; i<3; i++) {
        da[i]=da[i]/len;
    }
    double t=c[0]*da[0]+c[1]*da[1]+c[2]*da[2];
    c[0]=cos((double)d)*c[0]+(1-cos((double)d))*t*da[0]+sin((double)d)*(da[1]*c[2]-da[2]*c[1]);
    c[1]=cos((double)d)*c[1]+(1-cos((double)d))*t*da[1]+sin((double)d)*(da[2]*c[0]-da[0]*c[2]);
    c[2]=cos((double)d)*c[2]+(1-cos((double)d))*t*da[2]+sin((double)d)*(da[0]*c[1]-da[1]*c[0]);
}

void pitch(double d)
{
    double da[3],t[3];
    for (int i=0; i<3; i++) {
        t[i]=b[i]-a[i];
    }
    da[0]=(t[1]*c[2]-t[2]*c[1]);
    da[1]=t[2]*c[0]-t[0]*c[2];
    da[2]=t[0]*c[1]-t[1]*c[0];
    double len= getLength(da);
    //normalize
    for (int i=0; i<3; i++) {
        da[i]=da[i]/len;
    }
    double dd[3];
    for (int i=0; i<3; i++) {
        dd[i]=c[i];
    }
    double td=dd[0]*da[0]+dd[1]*da[1]+dd[2]*da[2];
    c[0]=cos((double)d)*dd[0]+(1-cos((double)d))*td*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    c[1]=cos((double)d)*dd[1]+(1-cos((double)d))*td*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    c[2]=cos((double)d)*dd[2]+(1-cos((double)d))*td*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);
    for (int i=0; i<3; i++) {
        dd[i]=t[i];
    }
    b[0]=a[0]+cos((double)d)*dd[0]+(1-cos((double)d))*td*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    b[1]=a[1]+cos((double)d)*dd[1]+(1-cos((double)d))*td*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    b[2]=a[2]+cos((double)d)*dd[2]+(1-cos((double)d))*td*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);
}

void animate(){
    //codes for any changes in Camera

    //cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!

    //codes for any changes in Models

    rectAngle -= 1;

    //MISSING SOMETHING? -- YES: add the following
    glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x,int y){
    switch(key){

        case '1':
            yaw(.05);
            break;
        case '2':
            yaw(-.05);
            break;
        case '3':
            roll(-.05);
            break;
        case '4':
            roll(.05);
            break;
        case '5':

            pitch(-.05);
            break;
        case '6':
            pitch(.05);
            break;
        case '7':	//increase rotation of camera by 10%
            roll(-.05);
            break;

        case '8':	//decrease rotation
            roll(.05);
            break;

        case '9':	//toggle grids
            canDrawGrid = 1 - canDrawGrid;
            break;
        case '=':
            walk_fly(1, true);
            break;
        case '-':
            walk_fly(-1, true);
            break;

        case 's':		//down arrow key
            walk_fly(-1, false);
            break;
        case 'w':		// up arrow key
            walk_fly(1, false);
            break;

        case 'd':
            starf(1);
            break;
        case 'a':
            starf(-1);
            break;
        default:
            break;
    }
}

void specialKeyListener(int key, int x,int y){
    switch(key){
//        case GLUT_KEY_DOWN:		//down arrow key
//            walk(-1);
//            break;
//        case GLUT_KEY_UP:		// up arrow key
//            walk(1);
//            break;

        case GLUT_KEY_RIGHT:
            starf(1);
            break;
        case GLUT_KEY_LEFT:
            starf(-1);
            break;

//        case GLUT_KEY_PAGE_UP:
//            fly(1);
//            break;
//        case GLUT_KEY_PAGE_DOWN:
//            fly(-1);
//            break;

        case GLUT_KEY_INSERT:
            break;
            
        case GLUT_KEY_HOME:
            break;
        case GLUT_KEY_END:
            break;
            
        default:
            break;
    }
}

void init(){
    //codes for initialization
    cameraAngle = 0;	//// init the cameraAngle
    cameraAngleDelta = 0.02;
    rectAngle = 0;
    height=20;
    canDrawGrid = true;
    r=50;
    
    a[0]=r*cos(cameraAngle);
    a[1]=r*sin(cameraAngle);
    a[2]=height;
    
    b[0]=b[1]=b[2]=0;
    
    c[0]=c[1]=0;
    c[2]=1;
    
    
    //clear the screen
    glClearColor(ASH, 0);
    
    /************************
     / set-up projection here
     ************************/
    //load the PROJECTION matrix
    glMatrixMode(GL_PROJECTION);
    
    //initialize the matrix
    glLoadIdentity();
    
    //give PERSPECTIVE parameters
    gluPerspective(70,	1,	0.1,	10000.0);
    //field of view in the Y (vertically)
    //aspect ratio that determines the field of view in the X direction (horizontally)
    //near distance
    //far distance
}
int main(int argc, char **argv){
    glutInit(&argc,argv);
    glutInitWindowSize(1000, 1000);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

    glutCreateWindow("My OpenGL Program");

    init();

    glEnable(GL_DEPTH_TEST);	//enable Depth Testing

    glutDisplayFunc(display);	//display callback function
    glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

    //ADD keyboard listeners:
    glutKeyboardFunc(keyboardListener);

    glutMainLoop();		//The main loop of OpenGL
    
    return 0;
}
